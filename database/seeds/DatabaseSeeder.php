<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'Mitu',
            'email' => 'mitu@gmail.com',
            'user_type' => '1',
            'password' => bcrypt('111111'),
        ]);
        DB::table('users')->insert([
            'name' => 'Sam',
            'email' => 'sam@gmail.com',
            'user_type' => '2',
            'password' => bcrypt('111111'),
        ]);
        DB::table('users')->insert([
            'name' => 'Sam2',
            'email' => 'sam2@gmail.com',
            'user_type' => '2',
            'password' => bcrypt('111111'),
        ]);
    }
}
