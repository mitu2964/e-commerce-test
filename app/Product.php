<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['id','title','category_id','supplier_id','description','image','status'];

    public function category(){
        return $this->hasOne('App\Category');
    }
}
