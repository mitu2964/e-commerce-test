
@extends('layouts.app')

@section('content')
    <h2>Product List</h2>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
{{--                    {{ dd($data) }}--}}
                    <div class="card-body">
                        {!! Form::model($data['product'],['url'=>['products/'.$data['product']->id],'role'=>'form','method'=>'PATCH','files'=>true]) !!}
                            @include('product.form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
