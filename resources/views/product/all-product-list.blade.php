@extends('layouts.app')

@section('content')
    <h2>All Product List</h2>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <table style="width:100%">
                            <tr>
                                <th>Title</th>
                                <th>description</th>
                                <th>img</th>
                                <th>Action</th>
                            </tr>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{ $product->title }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td><img src="{{asset( $product->image)}}" height="100" width="140"></td>
                                    <td>
                                        <li>
                                            <a class="btn btn-primary" href="{{url('products/'.$product->id.'/edit')}}" title="Edit">
                                                edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Delete">
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['products', $product->id],
                                                    'style' => 'display:inline'
                                                    ])
                                                !!}
                                                {!! Form::button('Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'action-edit-btn',
                                                        'onclick'=>'confirm("Want to delete?")'
                                                    ))
                                                !!}
                                                {!! Form::close() !!}
                                            </a>
                                        </li>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
