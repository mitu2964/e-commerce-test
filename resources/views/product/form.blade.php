<div class="form-group" style="max-width: 40%;">
    {!! Form::label('title','Title : ',['class'=>'control-label']) !!}
    {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'e.g. Shirt, Pant etc.']) !!}

    {!! Form::label('category_id','Category : ',['class'=>'control-label']) !!}

    <select class="form-control" name="category_id" id="category_id">
        @foreach($data['categories'] as $category)
            <option value="{{ $category->id }}">{{ $category->title }}</option>
        @endforeach
    </select>

    {!! Form::label('description','Description : ',['class'=>'control-label']) !!}
    {!! Form::textarea('description',null,['style'=>'margin-bottom:20px;','class'=>'form-control','placeholder'=>'e.g. Shirt, Pant etc.']) !!}

    {!! Form::label('image','Image : ',['class'=>'control-label']) !!}
    @if(empty($data['product']->image))
        {!! Form::file("image",["class" => "form-group"]) !!}
    @else
        <img src="{{ asset($data['product']->image) }}" height="100" width="150">
        {!! Form::file("image",["class" => "form-group"]) !!}
    @endif
    <input type="hidden" name="supplier_id" value="@if(Auth::check()) {{Auth::user()->id}} @endif">
</div>
<div class="margin-top-30">
    @if(empty($data['product']->id))
        <button type="submit" class="btn green">Save </button>
    @else
        <button type="submit" class="btn green">Update </button>
    @endif
    <a href="{{url('/categories')}}" class="btn default">Cancel </a>
</div>

