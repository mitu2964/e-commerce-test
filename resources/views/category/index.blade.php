@extends('layouts.app')

@section('content')
<h2>Category List</h2>
<a href="{{url('/categories/create')}}">Add Category</a>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <table style="width:100%">
                        <tr>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->title }}</td>
                                <td>
                                    <li>
                                        <a class="btn btn-primary" href="{{url('categories/'.$category->id.'/edit')}}" title="Edit">
                                            edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" title="Delete">
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['categories', $category->id],
                                                'style' => 'display:inline'
                                                ])
                                            !!}
                                            {!! Form::button('Delete', array(
                                                    'type' => 'submit',
                                                    'class' => 'action-edit-btn',
                                                    'onclick'=>'confirm("Want to delete?")'
                                                ))
                                            !!}
                                            {!! Form::close() !!}

                                        </a>

                                    </li>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
