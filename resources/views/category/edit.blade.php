
@extends('layouts.app')

@section('content')
    <h2>Category List</h2>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        {!! Form::model($data,['url'=>['categories/'.$data->id],'role'=>'form','method'=>'PATCH']) !!}
                            @include('category.form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
