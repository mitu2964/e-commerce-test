@extends('layouts.app')

@section('content')
    <h2>Category </h2>
    <a href="{{url('/categories')}}">Category List</a>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        {!! Form::open(['url'=>'/categories','role'=>'form','method'=>'POST']) !!}
                            @include('category.form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
