<div class="form-group" style="max-width: 40%;">
    {!! Form::label('title','Title : ',['class'=>'control-label']) !!}
    {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'e.g. Shirt, Pant etc.']) !!}
</div>
<div class="margin-top-30">
    @if(empty($data->id))
        <button type="submit" class="btn green">Save </button>
    @else
        <button type="submit" class="btn green">Update </button>
    @endif
    <a href="{{url('/categories')}}" class="btn default">Cancel </a>
</div>

